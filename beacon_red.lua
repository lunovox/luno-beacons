modWPBeacons.register_beacon("waypoint_beacons:red",{
	description = modWPBeacons.translate("Red Beacon"),
	protected = true,
	--light_height = 50,

	tiles_beacon = {"redbeacon.png"},
	tiles_base = {"redbase.png"},
	tiles_beam = {"redbeam.png"},

	node_base = "waypoint_beacons:redbase",
	node_beam = "waypoint_beacons:redbeam",
	
	particle = "redparticle.png",
	dye_collor = "dye:red",

	damage_per_second = 4 * 2,
	post_effect_color = {a=192, r=255, g=0, b=0},
	
	alias = {modWPBeacons.translate("beaconred"), "sinalizadorvermelho"},
})
